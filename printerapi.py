#!/usr/bin/env python

import array
import cli.app
import glob
import json
import os
import socket
import struct
import re
import uuid

# This is the fixed port that the printer API uses
TCP_PORT = 35

def MakeRequest(method, parameters):
    """ Create a Formlabs API request based on a method name and a parameters structure
    """
    req = {}
    req['Id'] = str(uuid.uuid4())
    req['Version'] = 1
    req['Method'] = method
    req['Parameters'] = parameters
    return req

def IsPrinterPrinting(s):
    # Check status of printer
    parameters = {}
    metadata = {}

    metadata = MakeRequest('PROTOCOL_METHOD_GET_STATUS', parameters)
    
    metadata_str = json.dumps(metadata)
    struct_fmt = '<I{}sQ'.format(len(metadata_str))
    start_print_packet = struct.pack(struct_fmt,
            len(metadata_str),
            metadata_str,
            0)

    s.send(start_print_packet)
    resp = s.recv(4096)

    # The json exists after the 4-byte json length and before the 8-byte binary length sections
    r = json.loads(resp[4:-8])

    # print r.keys()
    if r['Parameters']['isPrinting'] == False:
        print 'Printer is not printing'
        return False

    # if True not in r or r['isPrinting'] != True:
    #     print 'Printer is not printing'
    #     return False

    print 'Printer is printing'
    return True

def StartPrint(s, guid):
    """ Tell a specific printer to start printing the given
        job Guid. Note that the printer must already have the
        job uploaded before using this.
    """
    parameters = {}
    metadata = {}
    parameters['Guid'] = guid

    metadata = MakeRequest('PROTOCOL_METHOD_PRINT_JOB', parameters)

    metadata_str = json.dumps(metadata)
    struct_fmt = '<I{}sQ'.format(len(metadata_str))
    start_print_packet = struct.pack(struct_fmt,
            len(metadata_str),
            metadata_str,
            0)

    s.send(start_print_packet)
    resp = s.recv(4096)

    # The json exists after the 4-byte json length and before the 8-byte binary length sections
    r = json.loads(resp[4:-8])

    if 'Success' not in r or r['Success'] != True:
        print 'Failed to start the print: ' + r['Error']
        return False

    return True

def StartUpload(s, metadata, preview_image):
    """ Tell the printer to expect an upload, and give it the metadata for the job we're
        about to upload.
    """
    req = json.dumps(MakeRequest('PROTOCOL_METHOD_START_JOB', metadata), indent=4)

    if preview_image:
        binary_length = len(preview_image)
    else:
        binary_length = 0

    # Send the metadata
    s.send(struct.pack('<I', len(req)))
    s.send(req)
    # Followed by the binary length (0, in the case for the START_JOB request)
    s.send(struct.pack('<Q', binary_length))
    if binary_length > 0:
        s.send(preview_image)

    # Wait for a response.
    resp = s.recv(4096)

    # Strip the header, getting just the json info
    r = json.loads(resp[4:-8])

    # Ensure that the API call was successful
    if 'Success' not in r or r['Success'] != True:
        print 'Failed to start uploading job: ' + r['Error']
        return False

    return True

def UploadLayer(s, guid, layer_number, flx_file):
    """ Upload a specific FLX file to the printer.
    """
    print 'Uploading %s' %flx_file

    # Generate the metadata for the API call
    layer_metadata = {}
    layer_metadata['Guid'] = guid
    layer_metadata['Layer'] = layer_number
    metadata = json.dumps(MakeRequest('PROTOCOL_METHOD_UPLOAD_LAYER', layer_metadata), indent=4)

    # Read in the binary data. Note that this slurps the entire FLX file into memory.
    with open(flx_file, 'rb') as f:
        binary_data = f.read()

    s.send(struct.pack('<I', len(metadata)))
    s.send(metadata)
    s.send(struct.pack('<Q', len(binary_data)))
    s.send(binary_data)

    resp = s.recv(4096)
    r = json.loads(resp[4:-8])
    if 'Success' not in r or r['Success'] != True:
        return False

    return True

def UploadPrint(s, print_job_directory, metadata):
    """ Upload a job to the printer. Note that this assumes that the
        .flx files and Job.json are stored in the provided directory.
    """
    # If there is a preview image, it'll be in the job directory with the name 'preview.png'
    preview_image_file = os.path.join(print_job_directory, 'preview.png')
    # By default there is no preview image
    preview_image = None
    if os.path.exists(preview_image_file):
        with open(preview_image_file, 'rb') as f:
            preview_image = f.read()

    # First kick off the upload
    if not StartUpload(s, metadata, preview_image):
        print 'Failed to start upload, aborting...'
        return False

    # Now iterate through each layer file and upload it to the printer
    # The metadata file is assumed to be in the same directory as the FLX files.
    flx_files = sorted(glob.glob(os.path.join(print_job_directory, '*.flx')))
    num_layers = len(flx_files)
    print num_layers

    for n in range(0, num_layers):
        if not UploadLayer(s, metadata['Guid'], n, flx_files[n]):
            print 'Failed to upload %s' % flx_files[n]
            return False

    return True

@cli.app.CommandLineApp
def doPrinterThings(app):

    # Parameter checking
    if not app.params.upload and not app.params.start and not app.params.check:
        print 'No action given'
        return 1

    printer_ip = app.params.printer_ip
    print 'Using printer %s' % (printer_ip)

    # Parse metadata file
    metadata_file = os.path.join(app.params.print_job_directory, 'Job.json')
    with open(metadata_file) as f:
        metadata = json.load(f)
    guid = re.sub('[^0-9a-z\-]','', metadata['Guid'])

    # Connect to the printer
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((printer_ip, TCP_PORT))

    # Check and return status if check flag was set
    if app.params.check:
        if not IsPrinterPrinting(s):
            print 'Failed to check if printing or printer is not printing'

    # Upload FLX files if the upload flag was set
    if app.params.upload:
        if not UploadPrint(s, app.params.print_job_directory, metadata):
            print 'Failed to upload print'

    # start the print if the start_print flag was set
    if app.params.start:
        print 'Starting print guid %s' % (guid)
        if not StartPrint(s, guid):
            print 'Failed to start the print!'

    s.close()

doPrinterThings.add_param('-c', '--check', help='Check if printer is printing', default=False, action="store_true")
doPrinterThings.add_param('-s', '--start', help='Start the given print', default=False, action="store_true")
doPrinterThings.add_param('-u', '--upload', help='Upload given FLX files', default=False, action="store_true")
doPrinterThings.add_param('-i', '--printer_ip', help='IP Address of the printer', required=True)
doPrinterThings.add_param('print_job_directory', help='The directory containing FLX files and Job.json')

if __name__ == '__main__':
    doPrinterThings.run()
