# import paramiko
# import re

# json_file = open("status.txt", "r")
# json = json_file.read()
# print(json)
# regex = r"FINISHED"
# aux = re.search(regex, json)

# if aux:
# 	print aux.group(0)


# if json.find("FINISHED") > -1:
# 	print "hooray"
# else:
# 	print "damn"

# # ls -t | head -n1
# # find PATH -type f -printf "%T@ %p\n"| sort -nr
import array
import cli.app
import glob
import json
import os
import socket
import struct
import re
import uuid
import time

# global printerapi
import printerapi



import serial

TCP_PORT = 35
PRINTER_1 = '192.168.0.102'
PRINTER_2 = 'SOMETHING'
PRINTER_3 = 'SOMETHING'

def CheckPrinter(s):

	if not printerapi.IsPrinterPrinting(s):
		# print 'Failed to check if printing'
		return False

	return True

def SendToPrinter(s, print_job_directory):

	# Parse metadata file
    metadata_file = os.path.join(print_job_directory, 'Job.json')
    with open(metadata_file) as f:
        metadata = json.load(f)

    if not printerapi.UploadPrint(s, print_job_directory, metadata):
    	print 'Failed to upload print'
    	return False

	return True

def BeginPrint(s, print_job_directory):

	# Parse metadata file
    metadata_file = os.path.join(print_job_directory, 'Job.json')
    with open(metadata_file) as f:
        metadata = json.load(f)
    guid = re.sub('[^0-9a-z\-]','', metadata['Guid'])

    print 'Starting print guid %s' % (guid)
    if not printerapi.StartPrint(s, guid):
        print 'Failed to start the print!'
        return False

	return True

def monitor():

	# ser = serial.Serial()
	# ser.baudrate = 19200
	# ser.port = 'COM3'
	# ser.open()
	# print ser.is_open

	s1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s1.connect((PRINTER_1, TCP_PORT))

	# s2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	# s2.connect((PRINTER_2, TCP_PORT))

	# s3 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	# s3.connect((PRINTER_3, TCP_PORT))

	status = {}
	prev_status = {}

	prev_status['printer_1'] = CheckPrinter(s1)
	# prev_status['printer_2'] = CheckPrinter(s2)
	# prev_status['printer_3'] = CheckPrinter(s3)
	
	while True:

		# print 'hello'
		status['printer_1'] = CheckPrinter(s1)
		# status['printer_2'] = CheckPrinter(s2)
		# status['printer_3'] = CheckPrinter(s3)

		if status['printer_1'] == False and prev_status['printer_1'] == True:
			print 'Printer 1 finished'
			# Do stuff
			# ser.write(b'hello')

		# if status['printer_2'] == false and prev_status['printer_2'] == true:
			# print 'Printer 2 finished'
			# # Do stuff
			# ser.write(b'hello')

		# if status['printer_3'] == false and prev_status['printer_3'] == true:
			# print 'Printer 2 finished'
			# # Do stuff
			# ser.write(b'hello')

		prev_status['printer_1'] = status['printer_1']
		# prev_status['printer_2'] = status['printer_2']
		# prev_status['printer_3'] = status['printer_3']

		time.sleep(3)

monitor()